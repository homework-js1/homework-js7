
let array = ['hello', 'world', 23, '23', true, null, undefined, {id: 5656}];
const allTypes = ['string', 'number', 'boolean', 'null', 'undefined', 'object' ];

function filterBy(arr, type) {
    return arr.reduce((res, currentItem) => {
        if ( typeof currentItem !== type) {
            res.push(currentItem);
        }
        return res } , []);
}

allTypes.forEach(type => console.log(filterBy(array, type)));


// const filterBy = (arr, type) => arr.filter(item => typeof item !== type)
//
// allTypes.forEach(type => console.log(filterBy(array, type)));